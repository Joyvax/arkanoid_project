package com.example.arkanoid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends Activity {

	GameView Jeu;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Jeu = new GameView(this);
      
        setContentView(Jeu);
      
        Jeu.setOnTouchListener(
                new View.OnTouchListener() {
                
                
                public boolean onTouch(View myView, MotionEvent event) 
                {
                	if(Jeu.Quit)
                		finish();
                    return false;
                }
            });    

    }
 
  
    @Override
    protected void onDestroy() 
    {
    	super.onDestroy();
    }	

    @Override
    protected void onPause()
    {
    	super.onPause();
    }
    
    @Override
    protected void onResume()
    {
    	super.onResume();
    }
   
    
    @Override
    public void onBackPressed() {
      
    	//Message de confirmation pour sortir(builder section)
       	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Jeu.context);

			alertDialogBuilder.setTitle("Vous partez??");
			
			alertDialogBuilder
				.setMessage("�tes vous sur que vous voulez quitter")
				.setCancelable(false)
				.setPositiveButton("Oui",new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog,int id) 
					{
						MainActivity.this.finish();
					}
				})
				.setNegativeButton("Non",new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog,int id) 
					{
						dialog.cancel();
					}
				});
			
				//Creation du dialogued'alerte avec le builder
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
       
    }
}


