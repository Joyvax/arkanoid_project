package com.example.arkanoid;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

public class cAnimation
{
    private int FrameIndex = 0;
	private boolean IsLooping,isStopped;
	public boolean isFinish=false;
    private List<Bitmap> ListeImage = new ArrayList<Bitmap>();
    public Vector2D Position;
    private float FrameSpeed,SpeedMax = 1,CptSpeed;
    
    public boolean IsStopped()
    {                    
        return isStopped;
    }
	
    public void Stop()
    {
    	isStopped=true;
    }
    
    public void Continue()
    {
    	isStopped=false;
    }
    
    public void Restart()
    {
    	FrameIndex=0;
    }
    
    public cAnimation(boolean IsLooping,float FrameSpeed) 
    {
    	this.IsLooping = IsLooping;
    	this.FrameSpeed = FrameSpeed;
    	this.Position = new Vector2D();
    	CptSpeed = 0;
	}
    
    public cAnimation(boolean IsLooping,Vector2D Position,float FrameSpeed) 
    {
    	this.IsLooping = IsLooping;
    	this.Position = new Vector2D(Position.X, Position.Y);
    	this.FrameSpeed = FrameSpeed;
    	CptSpeed = 0;
	}
    
    public void AddImage(Context context,int IdImage)
    {
    	Bitmap Image = BitmapFactory.decodeResource(context.getResources(),IdImage);
    	ListeImage.add(Image);
    }
    
    public void Update()
    {
    	if(!isStopped)
    	{
    		CptSpeed += FrameSpeed;
    		
    		if(CptSpeed >= SpeedMax)
    		{
    			CptSpeed = 0;
    			if(IsLooping)
    			{
    				try
    				{
    					//Parcours de lanimation
    					FrameIndex = (FrameIndex + 1) % (ListeImage.size()-1); // le modulo fais un looping
    				}
    				catch(Error e)
    				{
    					FrameIndex = 0; // en cas d'erreur de traitement retours � l'image 0
    				}
    			}
    			else FrameIndex = Math.min(FrameIndex + 1, ListeImage.size() - 1);
    		}
    	}
    	
    	if(FrameIndex == ListeImage.size() - 1) isFinish = true; 
    	else isFinish = false;
    }
    
    public void Draw(Canvas canvas)
    {
    	 canvas.drawBitmap(ListeImage.get(FrameIndex), Position.X, Position.Y, null);
    }
}
