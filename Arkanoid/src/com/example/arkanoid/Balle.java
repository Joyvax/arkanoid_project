package com.example.arkanoid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceView;

public class Balle extends SurfaceView
{
	private Vector2D Position,Speed;
	private Bitmap Texture;//,PixelTest;
	private Rect RecBalle;
	private int LimiteXInf,LimiteXSupp,LimiteYInf,LimiteYSupp;
	private int InitialSpeedX,InitialSpeedY;
	public Boolean TouchBlock,SpeedPUActivate,Sticked, InRecStick,Delete;
	private int CptSpeedPU,SpeedPUDuration; //PowerUp Slow
	private boolean ApplySpeed,CalculateEcartStick;
	private float EcartStick;	
	private int Width;
	
	public int WidthLimite()
	{
		return Width;
	}
	
	public Vector2D PositionBalle()
	{
		return Position;
	}
	
	public Rect RecCollision()
	{
		return RecBalle;
	}
	
	public Vector2D SpeedBalle()
	{
		return Speed;
	}
	
	public Balle(Context context)
	{
		super(context);
	}
	
	public Balle(Context context,Vector2D Position,boolean PowerUp)
	{
		super(context);
	
		if(!PowerUp) this.Position = new Vector2D(Position.X + 55,Position.Y - 20);
		else this.Position = new Vector2D(Position.X,Position.Y);
		
		Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.balle1_1);
		//PixelTest = BitmapFactory.decodeResource(context.getResources(), R.drawable.pixeltest);
		RecBalle = new Rect((int)this.Position.X, (int)this.Position.Y, Texture.getWidth(), Texture.getHeight());

		InitialSpeedY = 4;
		InitialSpeedX = 0;
		
		Speed = new Vector2D(InitialSpeedX, InitialSpeedY);
		SpeedPUDuration = 200;
		CptSpeedPU = SpeedPUDuration;
		SpeedPUActivate = false;
		TouchBlock = false;
		ApplySpeed = false;
		
		if(!PowerUp)Sticked = true;
		else Sticked = false;
		
		InRecStick = false;
		Delete = false;
	}
	
	public void Update(Vaisseau_Arkanoid Vaisseau)
	{
		//#region Catch Power Up 
		if(!Sticked)
		{
			InRecStick = false;
			CalculateEcartStick = false;
		}
		//#endregion
		
		Position.Plus(Speed);
		
		RecBalle.left = (int)Position.X;
		RecBalle.top = (int)Position.Y;
		AutomaticChangePU();
		
		//#region Balle avec vaisseau
		if(Intersect(Vaisseau.RectCollision()))
		{
			if(Sticked)
			{
				InRecStick = true;
				Position.Y = Vaisseau.Pos().Y - 20;
				
				if(!CalculateEcartStick)
				{
					EcartStick = Position.X - Vaisseau.Pos().X;
					CalculateEcartStick = true;
				}
				
				Position.X = Vaisseau.Pos().X + EcartStick;
			}
			
			//Evite que la balle reste prise dans le vaisseau
			else if(Position.Y + RecBalle.bottom < Vaisseau.RectCollision().top + 15)
			{
				Speed.Y = -Speed.Y;
				Speed.Y -= 0.25;
				if(!Vaisseau.AucunDeplacement)
				{
					Speed.X += Vaisseau.Deplacement() / 2;
				}
				else
				{
					Rect r = Vaisseau.RectCollision();
					if(Position.X > r.left + r.right/2 - 8 && Position.X < r.left + r.right/2 + 8)
						Speed.X = 0;
				}
			}
		}
		//#endregion
		
		//#region Balle avec Block
		if(TouchBlock)
		{
			Speed.Y = -Speed.Y;
			Speed.Y += 0.20;
			TouchBlock = false;
		}
		//#endregion
		
		ApplicationLimites(Vaisseau);
	}

	public Boolean Intersect(Rect b)
	{		
		return (RecBalle.left <= b.right + b.left &&
			    b.left <= RecBalle.right + RecBalle.left &&
			    RecBalle.top <= b.bottom + b.top &&
			    b.top <= RecBalle.bottom + RecBalle.top);
	}
	
	public void draw(Canvas canvas)
	{
		canvas.drawBitmap(Texture,Position.X,Position.Y,null);
	/*
		canvas.drawBitmap(PixelTest, RecBalle.left, RecBalle.top,null);
		canvas.drawBitmap(PixelTest, RecBalle.left + RecBalle.right, RecBalle.top,null);
		canvas.drawBitmap(PixelTest, RecBalle.left, RecBalle.top + RecBalle.bottom,null);
		canvas.drawBitmap(PixelTest, RecBalle.left + RecBalle.right,RecBalle.top + RecBalle.bottom,null);	
	*/
	}
	
	public void SetLimite(int Width)
	{
		LimiteXInf = 0 + 30;
		LimiteXSupp = Width-25; 
		LimiteYInf = 0 + 30;
		LimiteYSupp = 1000;
		this.Width = Width;
	}
	
	private void ApplicationLimites(Vaisseau_Arkanoid Vaisseau)
	{
		//#region LIMITE Y
		if(Position.Y < LimiteYInf) 
		{
			Speed.Y = -Speed.Y;
			Speed.Y += 0.25;
		}
		else if(Position.Y > LimiteYSupp)
		{
			Delete = true;
			Vaisseau.Enlarge = false;
			Vaisseau.RectCollision().set(Vaisseau.RectCollision().left, Vaisseau.RectCollision().top, 128, Vaisseau.RectCollision().bottom);
		}
		//#endregion
		
		//#region LIMITE X 
		if(Position.X < LimiteXInf)
		{
			Position.X = LimiteXInf;
			Speed.X = Math.abs(Speed.X);
			Speed.X += 0.25;
		}
		else if (Position.X + RecBalle.right  > LimiteXSupp)
		{
			Position.X = LimiteXSupp - RecBalle.right;
			Speed.X = -Speed.X;
			Speed.X -= 0.25;
		}
		//#endregion
	}
	
	private void AutomaticChangePU()
	{
		//#region Power Up Slow 
		if(SpeedPUActivate)
		{
			if(!ApplySpeed)
			{
				Speed.Y /= 2;
				Speed.X /= 2;
				ApplySpeed = true;
			}
			
			CptSpeedPU--;
			if(CptSpeedPU < 0)
			{
				CptSpeedPU = SpeedPUDuration;
				Speed.Y *= 2;
				Speed.X *= 2;
				SpeedPUActivate = false;
				ApplySpeed = false;
			}
		}
		//#endregion
	}
	
	public void SetSpeed(Vector2D Speed)
	{
		this.Speed = new Vector2D(Speed.X,Speed.Y);
	}
}
