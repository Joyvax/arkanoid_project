package com.example.arkanoid;

import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Block
{
	protected Bitmap Texture;//,PixelTest;
	protected Vector2D Position;
	protected Rect RecBlock;
	public int NoBlock,NbTouche;
	public boolean Checked = false;
	private cAnimation Anim_SpecialBlock;
	private int BallInNumber;

	public Vector2D position()
	{
		return Position;
	}
	
	public Block(Context context,Vector2D Position,int NoBlock)
	{
		this.NoBlock = NoBlock;
		NbTouche = 0;
		switch(NoBlock)
		{						
			case 0: Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.block_1);
			break;
			
			case 1: Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.block_2);
			break;
			
			case 2: Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.block_3);
			break;
			
			case 3: Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.block_4);
			break;
			
			case 4: Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.block_5);
			break;
			
			case 5: Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.block_6);
			break;
			
			case 6: Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.block_7);
			break;
			
			case 7: Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.block_8);
			break;
			
			case 8: Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.specialbloc1_1);
			break;
		}
		
		//PixelTest = BitmapFactory.decodeResource(context.getResources(), R.drawable.pixeltest);
		this.Position = Position;
		RecBlock = new Rect((int)Position.X, (int)Position.Y, Texture.getWidth(), Texture.getHeight());
		Anim_SpecialBlock = new cAnimation(false,this.Position, 0.75f);
		Anim_SpecialBlock.AddImage(context, R.drawable.specialbloc1_1);
		Anim_SpecialBlock.AddImage(context, R.drawable.specialbloc1_2);
		Anim_SpecialBlock.AddImage(context, R.drawable.specialbloc1_3);
		Anim_SpecialBlock.AddImage(context, R.drawable.specialbloc1_4);
		Anim_SpecialBlock.AddImage(context, R.drawable.specialbloc1_5);
		Anim_SpecialBlock.AddImage(context, R.drawable.specialbloc1_6);
	}

	public void Update(List<Balle> ListBalle,Vaisseau_Arkanoid Vaisseau)
	{
		//#region Collision Balle Vaisseau block
		boolean Out = false;
		for (Iterator<Vaisseau_Balle> lVaisseauBalle = Vaisseau.ListeBalle.iterator(); lVaisseauBalle.hasNext() && !Out; ) 
    	{
			Vaisseau_Balle BalleShoot = lVaisseauBalle.next();
			if(BalleShoot.PositionB().Y < -20)
				lVaisseauBalle.remove();
			else if(BalleShoot.Intersect(RecBlock))
			{
				Out = true;
				lVaisseauBalle.remove();
				NbTouche++;
			}
    	}
		//#endregion

		if(NbTouche == 1)
		{
			Anim_SpecialBlock.Update();
		}
		
		//#region Si pas collision regarde avec la balle 
		if(!Out) 
		{
			int Index = 0;
			for (Iterator<Balle> IteratorListBalle = ListBalle.iterator(); IteratorListBalle.hasNext() && !Out; ) 
	    	{
				Balle balle = IteratorListBalle.next();
				
				if(balle.Intersect(RecBlock))
				{
					if(!Checked)
					{
						BallInNumber = Index;
						Checked = true;
						Out = true;
						balle.TouchBlock = true;
						NbTouche++;
					}
				}
				else
				{
					if(BallInNumber == Index)
					{
						Checked = false;
					}
				}
				
				Index++;
	    	}
		}
		//#endregion
	}
	
	public void draw(Canvas canvas)
	{
		canvas.drawBitmap(Texture, Position.X, Position.Y, null);
		if(NbTouche == 1)
		{
			Anim_SpecialBlock.Draw(canvas);
		}
		/*
		canvas.drawBitmap(PixelTest, RecBlock.left, RecBlock.top,null);
		canvas.drawBitmap(PixelTest, RecBlock.left + RecBlock.right, RecBlock.top,null);
		canvas.drawBitmap(PixelTest, RecBlock.left, RecBlock.top + RecBlock.bottom,null);
		canvas.drawBitmap(PixelTest, RecBlock.left + RecBlock.right,RecBlock.top + RecBlock.bottom,null);
		*/
	}
}
