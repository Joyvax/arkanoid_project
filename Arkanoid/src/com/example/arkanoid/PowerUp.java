package com.example.arkanoid;

import java.util.List;
import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;

public class PowerUp 
{
	private Vector2D Position,Speed;
	private cAnimation AnimationChoisi;
	private Random r;
	private int NoCapsule;
	private Rect RecPowerUp;
	//private Bitmap PixelTest;
	public Boolean Touched,NoCapsule5;

	public cAnimation Animation()
	{
		return AnimationChoisi;
	}
	
	public Vector2D PositionP()
	{
		return Position;
	}
	
	public PowerUp(Context context,Vector2D Position)
	{
		this.Position = Position;
		r = new Random();
		NoCapsule = r.nextInt(7);
		//PixelTest = BitmapFactory.decodeResource(context.getResources(), R.drawable.pixeltest);
		RessourceAnimation.Pu_Anim_Break.Position = this.Position;
		RessourceAnimation.Pu_Anim_Catch.Position = this.Position;
		RessourceAnimation.Pu_Anim_Disruption.Position = this.Position;
		RessourceAnimation.Pu_Anim_Enlarge.Position = this.Position;
		RessourceAnimation.Pu_Anim_Laser.Position = this.Position;
		RessourceAnimation.Pu_Anim_Player.Position = this.Position;
		RessourceAnimation.Pu_Anim_Slow.Position = this.Position;
		
		Speed = new Vector2D(0,5);
		RecPowerUp = new Rect((int)Position.X, (int)Position.Y, 60, 26);
		switch(NoCapsule)
		{
			case 0: AnimationChoisi = RessourceAnimation.Pu_Anim_Laser;
				break;
			case 1: AnimationChoisi = RessourceAnimation.Pu_Anim_Enlarge;
				break;
			case 2: AnimationChoisi = RessourceAnimation.Pu_Anim_Catch;
				break;
			case 3: AnimationChoisi = RessourceAnimation.Pu_Anim_Slow;
				break;
			case 4: AnimationChoisi = RessourceAnimation.Pu_Anim_Break;
				break;
			case 5: AnimationChoisi = RessourceAnimation.Pu_Anim_Disruption;
				break;
			case 6: AnimationChoisi = RessourceAnimation.Pu_Anim_Player;
				break;
		}
		
		Touched = false; NoCapsule5 = false;
	}
	
	public int UpdateLogic(Vaisseau_Arkanoid Vaisseau, List<Balle> Listballe, int NbLife)
	{
		RecPowerUp.left = (int)Position.X;
		RecPowerUp.top = (int)Position.Y;
		
		if(Vaisseau.Intersect(RecPowerUp))
		{
			Touched = true;
			switch(NoCapsule)
			{
				case 0: Vaisseau.Anim_EnCours = RessourceAnimation.V_Anim_ArkAAvion;
					break;
				case 1: Vaisseau.Anim_EnCours = RessourceAnimation.V_Anim_ArkPetitAGrand;
						Vaisseau.Enlarge = true;
					break;
				case 2: 
					for(int i=0; i < Listballe.size(); i++)
					{
						Listballe.get(i).Sticked = true;
					}		
					break;
				case 3: 
					for(int i=0; i < Listballe.size(); i++)
					{
						Listballe.get(i).SpeedPUActivate = true;
					}
					break;
				case 5: NoCapsule5 = true;
					break;
				case 6: NbLife++;
					break;			
			}
		}
		else
			Touched = false;
		
		return NbLife;
	}
	
	public void UpdateSpeedAndAnim()
	{
		Position.Plus(Speed);
		AnimationChoisi.Update();
	}
	
	public void draw(Canvas canvas)
	{
		AnimationChoisi.Draw(canvas);
		/*
		canvas.drawBitmap(PixelTest,RecPowerUp.left,RecPowerUp.top,null);
		canvas.drawBitmap(PixelTest,RecPowerUp.left + RecPowerUp.right,RecPowerUp.top,null);
		canvas.drawBitmap(PixelTest,RecPowerUp.left,RecPowerUp.top + RecPowerUp.bottom,null);
		canvas.drawBitmap(PixelTest,RecPowerUp.left + RecPowerUp.right,RecPowerUp.top + RecPowerUp.bottom,null);
		*/
	}
}
