package com.example.arkanoid;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Looper;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

class GameView extends SurfaceView implements SurfaceHolder.Callback {
    GameThread thread;
  
    int Screen_Width,Screen_Height;
    Context context;
    
    private Gestion_Map Map_Niveau1;
    private int[][] Grid_Niveau1 = 
    		{{2,0,2,4,7,5,6,1,3,1},
    	 {0,0,4,7,5,6,1,3,2,3},
    	 {2,4,0,5,6,1,3,2,3,1},
    	 {4,7,5,0,1,3,2,3,1,6},
    	 {7,5,6,1,0,2,3,1,6,5},
    	 {5,6,1,3,2,0,1,6,5,7},
    	 {6,1,3,2,3,1,0,5,7,4},
    	 {1,3,2,3,1,6,5,0,4,2},
    	 {3,2,3,1,6,5,7,4,0,0},
    	 {8,8,8,8,8,8,8,8,8,8}};
    	// {1,3,1,6,5,7,4,2,0,2}};
    private Vaisseau_Arkanoid Vaisseau_Player1;
    private List<Balle> List_Ball; //List for the PU
    private Bitmap Bitmap_GameOver;
    private cButton Button_Continuer,Button_Quitter,Button_Recommencer;
    int FoisRecommencer;
    boolean Quit;
    
    public GameView(Context context) 
    {
        super(context);
        this.context = context;
        //Set thread
        getHolder().addCallback(this);
        setFocusable(true);
        RessourceAnimation.LoadAnimation(context);
        Map_Niveau1 = new Gestion_Map(context, 1, 1, Grid_Niveau1);
        Vaisseau_Player1 = new Vaisseau_Arkanoid(context, new Vector2D(300,900));
        
        List_Ball = new ArrayList<Balle>();
        List_Ball.add(new Balle(context, Vaisseau_Player1.Pos(),false));
        Bitmap_GameOver = BitmapFactory.decodeResource(context.getResources(), R.drawable.gameover);
        Button_Continuer = new cButton(context, R.drawable.continuer, 200, 400, 360, 120);
        Button_Quitter = new cButton(context, R.drawable.quitter, 200, 600, 360, 120);
        Button_Recommencer = new cButton(context, R.drawable.recommencer, 200, 400,360,120);
        Quit = false;
    }

    @Override
    public void onSizeChanged (int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        //This event-method provides the real dimensions of this custom view.
        Screen_Height = h;
        Screen_Width = w;
        
        Map_Niveau1.ResizeBackground(w, h);
        Vaisseau_Player1.SetLimite(w);
        for(Balle b:List_Ball)
        	b.SetLimite(w);
    }

    //***************************************
    //				  TOUCH  
    //***************************************
 
    public synchronized boolean onTouchEvent(MotionEvent ev) 
    {
    	if(Map_Niveau1.NbLife != 0)
    		Vaisseau_Player1.Update(ev,List_Ball);
    	else
    	{
    		if(FoisRecommencer == 0)
    			Button_Continuer.Update(context, ev);
    		else
    			Button_Recommencer.Update(context, ev);
    		
    		Button_Quitter.Update(context, ev);
    		
    		if(FoisRecommencer == 0)
    		{
    			if(Button_Continuer.IsClicked)
    			{
    				Toast.makeText(context, "Vous recommencerez au " + System.getProperty("line.separator") + "prochain Game Over", Toast.LENGTH_SHORT).show();
    				Map_Niveau1.NbLife = 3;
    				FoisRecommencer++;
    			}
    		}
    		else 
    		{
    			 Map_Niveau1 = new Gestion_Map(context, 1, 1, Grid_Niveau1);
    			 Map_Niveau1.ResizeBackground(Screen_Width, Screen_Height);
    		}
    	
    		if(Button_Quitter.IsClicked)
    		{
    			Quit = true;
    		}
    	}
        return true;
    }

    @Override
    public void draw(Canvas canvas) 
    {
        super.draw(canvas);  
        if(Map_Niveau1.NbLife != 0)
        {
        	Map_Niveau1.Update(context,List_Ball,Vaisseau_Player1);
        	Map_Niveau1.draw(canvas);
        	Vaisseau_Player1.draw(canvas,context);
        
	        //#region Power Up Explosion Balle
        	if(List_Ball.size() == 0)
        	{
                List_Ball.add(new Balle(context, Vaisseau_Player1.Pos(),false));
                List_Ball.get(0).SetLimite(Screen_Width);
                Map_Niveau1.NbLife--;
        	}
	        //#endregion
        	
	        for(Balle b:List_Ball)
	        {
	        	b.Update(Vaisseau_Player1);
	        	b.draw(canvas);
	        }
        }
        else
        {
        	canvas.drawBitmap(Map_Niveau1.Background,0,0,null);
        	canvas.drawBitmap(Map_Niveau1.Tuyauterie,0,0,null);
        	canvas.drawBitmap(Bitmap_GameOver, 0, 0, null);
        	
        	if(FoisRecommencer == 0)
        		Button_Continuer.draw(canvas);
        	else
        		Button_Recommencer.draw(canvas);
        	Button_Quitter.draw(canvas);
        }
    }
    
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) 
    {
    	
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) 
    {
        thread = new GameThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();
    }
    
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) 
    {
        boolean retry = true;
        thread.setRunning(false);
   
        while (retry)
        {
            try 
            {
                thread.join();
                retry = false;
            } 
            
            catch (InterruptedException e) {     }
        }
    }


    class GameThread extends Thread 
    {
        private SurfaceHolder surfaceHolder;
        private GameView gameView;
        private boolean run = false;

        public GameThread(SurfaceHolder surfaceHolder, GameView gameView) 
        {
            this.surfaceHolder = surfaceHolder;
            this.gameView = gameView;
        }

        public void setRunning(boolean run) 
        {
            this.run = run;
        }

        public SurfaceHolder getSurfaceHolder() 
        {
            return surfaceHolder;
        }
        
        
        @Override
        public void run() 
        {
        	Looper.prepare();
            Canvas c;
            while (run) 
            {
                c = null;
                try 
                {
                	c = surfaceHolder.lockCanvas(null);
                    synchronized (surfaceHolder)
                    {
                    	if(c!=null)
                        gameView.draw(c);
                    }
                } 
                finally 
                {
                    if (c != null) {surfaceHolder.unlockCanvasAndPost(c);}
                }
            }                 
        }
    }
}