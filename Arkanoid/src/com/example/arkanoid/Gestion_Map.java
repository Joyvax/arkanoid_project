package com.example.arkanoid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.text.TextPaint;

public class Gestion_Map 
{
	//Niveau
    public Bitmap Background,Tuyauterie;
    private List<Block> ListeBlocks = new ArrayList<Block>();
    private Vector2D PositionInitialeBlock = new Vector2D(37,20);
    private PowerUp PowerUp;
    private Random rBlock;
    private int NbBlockPU,FrequencePowerUp;
    
    private TextPaint TextPaint;
    private Vector2D PositionLife;
    public int NbLife;
    private Bitmap BLife;
    
	public Gestion_Map(Context context, int BgrNo,int TuyauNo,int[][] Grille)
	{
		//Background
		switch(BgrNo)
		{
			case 1:  Background = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_01);
			break;
 		}
		
		//Tuyauterie
		switch (TuyauNo) 
		{
			case 1: Tuyauterie = BitmapFactory.decodeResource(context.getResources(), R.drawable.tuyauterie_1);
			break;
		}
		
		//Bloc
		for ( int y=0; y< Grille.length; y++)
		{	
			for( int x=0; x< Grille[y].length; x++)
			{	
				ListeBlocks.add(new Block(context, new Vector2D(PositionInitialeBlock.X + x * 65, PositionInitialeBlock.Y + y * 35),Grille[y][x]));
			}
		}
		
		//Power Up�
		FrequencePowerUp = 5;
		rBlock = new Random();
		SetPowerUpNumberBlock();
		TextPaint = new TextPaint();
		TextPaint.setColor(Color.WHITE);
		TextPaint.setTextSize(50);
		NbLife = 3;
		PositionLife = new Vector2D(400, 1100);
		BLife = BitmapFactory.decodeResource(context.getResources(), R.drawable.balle1_1);
	}
	
	public void Update(Context context, List<Balle> ListBalle, Vaisseau_Arkanoid Vaisseau)
	{
		//#region PowerUp 
		if(PowerUp != null)
		{
			PowerUp.UpdateSpeedAndAnim();
			
				NbLife = PowerUp.UpdateLogic(Vaisseau,ListBalle,NbLife);
			
			if(PowerUp.PositionP().Y > 1200)
				PowerUp = null;
			
			else if (PowerUp.Touched) 
			{
				if(PowerUp.NoCapsule5)
				{
					//D�couplement des balles(explosion)
					Balle b = ListBalle.get(ListBalle.size()-1);
					ListBalle.add(new Balle(context,b.PositionBalle(),true));
					ListBalle.get(ListBalle.size() - 1).SetLimite(b.WidthLimite());
					ListBalle.get(ListBalle.size() - 1).SetSpeed(new Vector2D(b.SpeedBalle().X + 5,b.SpeedBalle().Y));
					ListBalle.add(new Balle(context,b.PositionBalle(),true));
					ListBalle.get(ListBalle.size() - 1).SetLimite(b.WidthLimite());
					ListBalle.get(ListBalle.size() - 1).SetSpeed(new Vector2D(b.SpeedBalle().X - 5,b.SpeedBalle().Y));
				}	
				PowerUp = null;
			}
		}
		//#endregion
		
		//#region ball
		for (Iterator<Balle> LBalle = ListBalle.iterator(); LBalle.hasNext(); ) 
		{
			Balle b = LBalle.next();
			if(b.Delete) LBalle.remove();
		}
		//#endregion
		
		//#region Check Block destroy 
		for (Iterator<Block> LBlock = ListeBlocks.iterator(); LBlock.hasNext(); ) 
    	{
    		Block block = LBlock.next();
    		block.Update(ListBalle,Vaisseau);
  
    		if (block.NoBlock != 8) 
    	    {
    			if(block.NbTouche == 1)
    			{
    				CreatePowerUp(context,block,LBlock);
    			}
    	    }
    		else
    		{
    			if(block.NbTouche == 2)
    			{
    				CreatePowerUp(context,block,LBlock);
    			}
    		}
    	    
    	}
		//#endregion
	}
	
	public void ResizeBackground(int width,int height)
	{
		Background = Bitmap.createScaledBitmap(Background, width, height, true);
	}
	
	private void SetPowerUpNumberBlock()
	{
		NbBlockPU = rBlock.nextInt(FrequencePowerUp);
	}
	
	public void draw(Canvas canvas)
	{
		canvas.drawBitmap(Background,0,0,null);
		for(Block b: ListeBlocks)
		{
			b.draw(canvas);
		}
		canvas.drawBitmap(Tuyauterie,0,0,null);
		
		if(PowerUp != null)
			PowerUp.draw(canvas);
		
		canvas.drawText("LIFE", PositionLife.X, PositionLife.Y, TextPaint);
		for(int i=0; i<NbLife; i++)
		{
			//Dessin des vies en lign�e de 6 vies
			if (i > 17)
				canvas.drawBitmap(BLife,PositionLife.X + 120 + ((i-18) *25), PositionLife.Y + 50,null);
			
			if (i > 11)
				canvas.drawBitmap(BLife,PositionLife.X + 120 + ((i-12) *25), PositionLife.Y + 25,null);
			
			else if (i > 5)
				canvas.drawBitmap(BLife,PositionLife.X + 120 + ((i-6) *25), PositionLife.Y,null);
			
			else
				canvas.drawBitmap(BLife,PositionLife.X + 120 + (i *25), PositionLife.Y - 25,null);
		}
	}
	
	private void CreatePowerUp(Context context, Block block,Iterator<Block>LBlock)
	{
		//Create PowerUp when reached the number of block
		if(PowerUp == null && NbBlockPU <= 0)
		{
			PowerUp = new PowerUp(context, block.position());
			SetPowerUpNumberBlock();
		}

		LBlock.remove();
		NbBlockPU--;
	}
}
