package com.example.arkanoid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Vaisseau_Balle 
{
	private Vector2D Position;
	private Vector2D Speed;
	private Bitmap Texture;//,PixelTest;
	private Rect RecVBalle;
	
	public Vector2D PositionB()
	{
		return Position;
	}
	
	public Rect RecCollision()
	{
		return RecVBalle;
	}
	
	public Vaisseau_Balle(Context context,Vector2D Position)
	{
		this.Position = new Vector2D(Position.X ,Position.Y);
		Texture = BitmapFactory.decodeResource(context.getResources(), R.drawable.vaisseauballe_1);
		//PixelTest = BitmapFactory.decodeResource(context.getResources(), R.drawable.pixeltest);
		Speed = new Vector2D(0,7);
		
		RecVBalle = new Rect((int)this.Position.X, (int)this.Position.Y, 10, 30);
	}
	
	public void Update()
	{
		Position.Moins(Speed);
		RecVBalle.left = (int)Position.X;
		RecVBalle.top = (int)Position.Y;
	}
	
	public boolean Intersect(Rect b)
	{
		return (RecVBalle.left <= b.right + b.left &&
			    b.left <= RecVBalle.right + RecVBalle.left &&
			    RecVBalle.top <= b.bottom + b.top &&
			    b.top <= RecVBalle.bottom + RecVBalle.top);
	}
	
	public void draw(Canvas canvas)
	{
		canvas.drawBitmap(Texture, Position.X, Position.Y, null);
		
		/*
		canvas.drawBitmap(PixelTest,RecVBalle.left,RecVBalle.top,null);
		canvas.drawBitmap(PixelTest,RecVBalle.left + RecVBalle.right,RecVBalle.top,null);
		canvas.drawBitmap(PixelTest,RecVBalle.left,RecVBalle.top + RecVBalle.bottom,null);
		canvas.drawBitmap(PixelTest,RecVBalle.left + RecVBalle.right,RecVBalle.top + RecVBalle.bottom,null);
		*/
	}
	
}
