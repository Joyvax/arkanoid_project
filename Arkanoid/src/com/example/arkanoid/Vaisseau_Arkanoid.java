package com.example.arkanoid;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;

public class Vaisseau_Arkanoid
{
	public cAnimation Anim_EnCours;
	private Vector2D Position,AnciennePosition;
	private int LimiteXInf,LimiteXSupp;
	private int SpeedToReach;
	private Rect RecArk;
	private float DeplacementX;
	public Boolean AucunDeplacement,Enlarge;
	private int AutomaticSwitch;
	private int FrequenceTir,CptTir;
	public List<Vaisseau_Balle> ListeBalle;
	//private Bitmap PixelTest;
	
	public Vector2D Pos()
	{
		return AnciennePosition;
	}
	
	public Rect RectCollision()
	{
		return RecArk;
	}
	
	public float Deplacement()
	{
		return DeplacementX;
	}
	
	public Vaisseau_Arkanoid(Context context,Vector2D Position)
	{
		this.Position = Position;
		AnciennePosition = new Vector2D(Position.X,Position.Y);
		SpeedToReach = 10;
		RecArk = new Rect((int)Position.X, (int)Position.Y, 128, 32);
		Anim_EnCours = RessourceAnimation.V_Anim_ArkPetit;
		AutomaticSwitch = 0;
		ListeBalle = new ArrayList<Vaisseau_Balle>();
		CptTir = 0;
		FrequenceTir = 30;
		//PixelTest = BitmapFactory.decodeResource(context.getResources(), R.drawable.pixeltest);
		Enlarge = false;
	}
	
	public void Update(MotionEvent ev,List<Balle> ListBalle)
	{
		switch (ev.getAction()) 
		{
			case MotionEvent.ACTION_DOWN:
	        {
	        	if(Anim_EnCours != RessourceAnimation.V_Anim_ArkGrand)
	        		Position.X = (int)ev.getX() - ((RecArk.right/4)*3);
	        	else
	        		Position.X = (int)ev.getX() - RecArk.right;
	        	break;
	        }
			case MotionEvent.ACTION_UP:
			{
				for (Balle b: ListBalle)
				{
					if(b.InRecStick)
					{
						b.Sticked = false;
						Anim_EnCours = RessourceAnimation.V_Anim_ArkPetit;
					}
				}
			}
	    }
	}
	
	public void draw(Canvas canvas,Context context)
	{
		ReachPosition();
		Anim_EnCours.Update();
		Animation_AutomaticSwitch();
		
		/********* Shooting section (PowerUp) *******/
		if(Anim_EnCours == RessourceAnimation.V_Anim_Avion)
			Shoot(context);
		
		for(Vaisseau_Balle b: ListeBalle)
		{
			b.Update();
			b.draw(canvas);
		}
		/*******************************************/
		
		Anim_EnCours.Draw(canvas);
		/*
		canvas.drawBitmap(PixelTest,RecArk.left,RecArk.top,null);
		canvas.drawBitmap(PixelTest,RecArk.left + RecArk.right,RecArk.top,null);
		canvas.drawBitmap(PixelTest,RecArk.left,RecArk.top + RecArk.bottom,null);
		canvas.drawBitmap(PixelTest,RecArk.left + RecArk.right,RecArk.top + RecArk.bottom,null);
		*/
	}
	
	public void SetLimite(int Width)
	{
		LimiteXInf = 0 + 30;
		LimiteXSupp = Width - 150; 
	}
	
	/**** Go to next Position ***/
	private void ReachPosition()
	{
		AucunDeplacement = false;
		
		if(AnciennePosition.X < Position.X - SpeedToReach)
			AnciennePosition.X += SpeedToReach;
		else if (AnciennePosition.X > Position.X + SpeedToReach)
			AnciennePosition.X -= SpeedToReach;
		else
			AucunDeplacement = true;
		
		//Limite
		if(AnciennePosition.X < LimiteXInf) AnciennePosition.X = LimiteXInf;
		else if(AnciennePosition.X > LimiteXSupp)
		{
			AnciennePosition.X = LimiteXSupp;
		}
	
		RecArk.left = (int)AnciennePosition.X;
		Anim_EnCours.Position = AnciennePosition;
		
		if(!AucunDeplacement)
			DeplacementX = (Position.X - AnciennePosition.X)/50;
	}
	
	public boolean Intersect(Rect b)
	{
		return (RecArk.left <= b.right + b.left &&
			    b.left <= RecArk.right + RecArk.left &&
			    RecArk.top <= b.bottom + b.top &&
			    b.top <= RecArk.bottom + RecArk.top);
	}
	
	public void Shoot(Context context)
	{
		CptTir++;
		if(CptTir >= FrequenceTir)
		{
			ListeBalle.add(new Vaisseau_Balle(context, new Vector2D(AnciennePosition.X + 30,AnciennePosition.Y)));
			ListeBalle.add(new Vaisseau_Balle(context, new Vector2D(AnciennePosition.X + 85,AnciennePosition.Y)));
			CptTir = 0;
		}
	}
	
	private void Animation_AutomaticSwitch()
	{
		/**********************************
		   Compteur de changement pour les  
		       Animations automatiques  
		***********************************/
		if(AutomaticSwitch >= -1)
		AutomaticSwitch--;
		
		//Ark A Avion
		if(Anim_EnCours == RessourceAnimation.V_Anim_ArkAAvion && Anim_EnCours.isFinish)
		{
			RessourceAnimation.V_Anim_ArkAAvion.Restart();
			Anim_EnCours = RessourceAnimation.V_Anim_Avion;
			AutomaticSwitch = 200;
			if(RecArk.right != 128)
				RecArk.set(RecArk.left, RecArk.top, 128, RecArk.bottom);
		}
		
		//Avion A Ark
		if(Anim_EnCours == RessourceAnimation.V_AnimAvionAArk && Anim_EnCours.isFinish)
		{
			RessourceAnimation.V_AnimAvionAArk.Restart(); 
			Anim_EnCours = RessourceAnimation.V_Anim_ArkPetit;
			RecArk.set(RecArk.left, RecArk.top, 128, RecArk.bottom);
		}
		
		//Ark Petit A Grand
		if(Anim_EnCours == RessourceAnimation.V_Anim_ArkPetitAGrand && Anim_EnCours.isFinish)
		{
			RessourceAnimation.V_Anim_ArkPetitAGrand.Restart();
			Anim_EnCours = RessourceAnimation.V_Anim_ArkGrand;
			RecArk.set(RecArk.left, RecArk.top, 192, RecArk.bottom);
		}
		
		////Ark Grand A Petit
		if(Anim_EnCours == RessourceAnimation.V_Anim_ArkGrandAPetit && Anim_EnCours.isFinish)
		{
			RessourceAnimation.V_Anim_ArkGrandAPetit.Restart();
			Anim_EnCours = RessourceAnimation.V_Anim_ArkPetit;
			if(RecArk.right != 128)
				RecArk.set(RecArk.left, RecArk.top, 128, RecArk.bottom);
		}
		
		if(Anim_EnCours == RessourceAnimation.V_Anim_ArkGrand && !Enlarge)
		{
			Anim_EnCours = RessourceAnimation.V_Anim_ArkGrandAPetit;
		}
		/////////////////////
		
		if(AutomaticSwitch == 0)
		{
			//Avion transformation pour etre Ark
			if(Anim_EnCours == RessourceAnimation.V_Anim_Avion)
				Anim_EnCours = RessourceAnimation.V_AnimAvionAArk;
		}
	}
	
}
