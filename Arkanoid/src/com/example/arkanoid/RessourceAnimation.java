package com.example.arkanoid;

import android.content.Context;

public class RessourceAnimation 
{
	//Vaisseau
	public static cAnimation V_Anim_ArkPetit,V_Anim_ArkGrand,
	V_Anim_ArkGrandAPetit,V_Anim_ArkPlusPetit,V_Anim_ArkPetitAGrand,
	V_Anim_ArkInvincible,V_Anim_ArkAAvion,V_Anim_Avion,V_AnimAvionAArk,V_Anim_ArkDead,
	V_Anim_ArkNewLife;
	
	//Power Up
	public static cAnimation Pu_Anim_Laser,Pu_Anim_Enlarge,Pu_Anim_Catch,
	Pu_Anim_Slow,Pu_Anim_Break,Pu_Anim_Disruption,Pu_Anim_Player;
	
	//BlockSpecial
	public static cAnimation Bl_Anim_Touched;
	
	static public void LoadAnimation(Context context)
	{
		//Vaisseau
		V_Anim_ArkPetit = new cAnimation(true, 0.08f);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau1_1);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau1_2);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau1_3);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau1_4);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau2_1);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau2_2);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau2_3);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau2_4);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau3_1);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau3_2);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau3_3);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau3_4);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau4_1);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau4_2);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau4_3);
		V_Anim_ArkPetit.AddImage(context, R.drawable.vaisseau4_4);
		V_Anim_ArkGrand = new cAnimation(true, 0.08f);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau5_1);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau5_2);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau5_3);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau5_4);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau6_1);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau6_2);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau6_3);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau6_4);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau7_1);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau7_2);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau7_3);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau7_4);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau8_1);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau8_2);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau8_3);
		V_Anim_ArkGrand.AddImage(context, R.drawable.vaisseau8_4);
		V_Anim_ArkGrandAPetit = new cAnimation(false, 0.3f);
		V_Anim_ArkGrandAPetit.AddImage(context, R.drawable.vaisseau11_3);
		V_Anim_ArkGrandAPetit.AddImage(context, R.drawable.vaisseau11_2);
		V_Anim_ArkGrandAPetit.AddImage(context, R.drawable.vaisseau11_1);
		V_Anim_ArkPlusPetit = new cAnimation(true, 0.08f);
		V_Anim_ArkPlusPetit.AddImage(context, R.drawable.vaisseau10_1);
		V_Anim_ArkPlusPetit.AddImage(context, R.drawable.vaisseau10_2);
		V_Anim_ArkPlusPetit.AddImage(context, R.drawable.vaisseau10_3);
		V_Anim_ArkPlusPetit.AddImage(context, R.drawable.vaisseau10_4);
		V_Anim_ArkPetitAGrand = new cAnimation(false, 0.3f);
		V_Anim_ArkPetitAGrand.AddImage(context, R.drawable.vaisseau11_1);
		V_Anim_ArkPetitAGrand.AddImage(context, R.drawable.vaisseau11_2);
		V_Anim_ArkPetitAGrand.AddImage(context, R.drawable.vaisseau11_3);
		V_Anim_ArkInvincible = new cAnimation(true, 0.05f);
		V_Anim_ArkInvincible.AddImage(context, R.drawable.vaisseau12_1);
		V_Anim_ArkInvincible.AddImage(context, R.drawable.vaisseau12_2);
		V_Anim_ArkInvincible.AddImage(context, R.drawable.vaisseau12_3);
		V_Anim_ArkAAvion = new cAnimation(false, 0.5f);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_1);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_2);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_3);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_4);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_5);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_6);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_7);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_8);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_9);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_10);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_11);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_12);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_13);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_14);
		V_Anim_ArkAAvion.AddImage(context, R.drawable.vaisseau13_15);
		V_Anim_Avion = new cAnimation(true, 0.08f);
		V_Anim_Avion.AddImage(context, R.drawable.vaisseau14_1);
		V_Anim_Avion.AddImage(context, R.drawable.vaisseau14_2);
		V_Anim_Avion.AddImage(context, R.drawable.vaisseau14_3);
		V_Anim_Avion.AddImage(context, R.drawable.vaisseau14_4);
		V_AnimAvionAArk = new cAnimation(false, 0.5f);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_15);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_14);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_13);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_12);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_11);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_10);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_9);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_8);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_7);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_6);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_5);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_4);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_3);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_2);
		V_AnimAvionAArk.AddImage(context, R.drawable.vaisseau13_1);
		V_Anim_ArkDead = new cAnimation(false, 0.08f);
		V_Anim_ArkDead.AddImage(context, R.drawable.vaisseau15_1);
		V_Anim_ArkDead.AddImage(context, R.drawable.vaisseau15_2);
		V_Anim_ArkDead.AddImage(context, R.drawable.vaisseau15_3);
		V_Anim_ArkDead.AddImage(context, R.drawable.vaisseau15_4);
		V_Anim_ArkNewLife = new cAnimation(false, 0.16f);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau16_1);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau16_2);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau16_3);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau16_4);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau17_1);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau17_2);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau17_3);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau17_4);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau18_1);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau18_2);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau18_3);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau18_4);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau19_1);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau19_2);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau19_3);
		V_Anim_ArkNewLife.AddImage(context, R.drawable.vaisseau19_4);
		
		//Power Up
		Pu_Anim_Laser = new cAnimation(true, 0.15f);
		Pu_Anim_Laser.AddImage(context, R.drawable.powerup1_1);
		Pu_Anim_Laser.AddImage(context, R.drawable.powerup1_2);
		Pu_Anim_Laser.AddImage(context, R.drawable.powerup1_3);
		Pu_Anim_Laser.AddImage(context, R.drawable.powerup1_4);
		Pu_Anim_Laser.AddImage(context, R.drawable.powerup1_5);
		Pu_Anim_Laser.AddImage(context, R.drawable.powerup1_6);
		Pu_Anim_Laser.AddImage(context, R.drawable.powerup1_7);
		Pu_Anim_Laser.AddImage(context, R.drawable.powerup1_8);
		Pu_Anim_Enlarge = new cAnimation(true, 0.15f);
		Pu_Anim_Enlarge.AddImage(context, R.drawable.powerup2_1);
		Pu_Anim_Enlarge.AddImage(context, R.drawable.powerup2_2);
		Pu_Anim_Enlarge.AddImage(context, R.drawable.powerup2_3);
		Pu_Anim_Enlarge.AddImage(context, R.drawable.powerup2_4);
		Pu_Anim_Enlarge.AddImage(context, R.drawable.powerup2_5);
		Pu_Anim_Enlarge.AddImage(context, R.drawable.powerup2_6);
		Pu_Anim_Enlarge.AddImage(context, R.drawable.powerup2_7);
		Pu_Anim_Enlarge.AddImage(context, R.drawable.powerup2_8);
		Pu_Anim_Catch = new cAnimation(true, 0.15f);
		Pu_Anim_Catch.AddImage(context, R.drawable.powerup3_1);
		Pu_Anim_Catch.AddImage(context, R.drawable.powerup3_2);
		Pu_Anim_Catch.AddImage(context, R.drawable.powerup3_3);
		Pu_Anim_Catch.AddImage(context, R.drawable.powerup3_4);
		Pu_Anim_Catch.AddImage(context, R.drawable.powerup3_5);
		Pu_Anim_Catch.AddImage(context, R.drawable.powerup3_6);
		Pu_Anim_Catch.AddImage(context, R.drawable.powerup3_7);
		Pu_Anim_Catch.AddImage(context, R.drawable.powerup3_8);
		Pu_Anim_Slow = new cAnimation(true, 0.15f);
		Pu_Anim_Slow.AddImage(context, R.drawable.powerup4_1);
		Pu_Anim_Slow.AddImage(context, R.drawable.powerup4_2);
		Pu_Anim_Slow.AddImage(context, R.drawable.powerup4_3);
		Pu_Anim_Slow.AddImage(context, R.drawable.powerup4_4);
		Pu_Anim_Slow.AddImage(context, R.drawable.powerup4_5);
		Pu_Anim_Slow.AddImage(context, R.drawable.powerup4_6);
		Pu_Anim_Slow.AddImage(context, R.drawable.powerup4_7);
		Pu_Anim_Slow.AddImage(context, R.drawable.powerup4_8);
		Pu_Anim_Break = new cAnimation(true, 0.15f);
		Pu_Anim_Break.AddImage(context, R.drawable.powerup5_1);
		Pu_Anim_Break.AddImage(context, R.drawable.powerup5_2);
		Pu_Anim_Break.AddImage(context, R.drawable.powerup5_3);
		Pu_Anim_Break.AddImage(context, R.drawable.powerup5_4);
		Pu_Anim_Break.AddImage(context, R.drawable.powerup5_5);
		Pu_Anim_Break.AddImage(context, R.drawable.powerup5_6);
		Pu_Anim_Break.AddImage(context, R.drawable.powerup5_7);
		Pu_Anim_Break.AddImage(context, R.drawable.powerup5_8);
		Pu_Anim_Disruption = new cAnimation(true, 0.15f);
		Pu_Anim_Disruption.AddImage(context, R.drawable.powerup6_1);
		Pu_Anim_Disruption.AddImage(context, R.drawable.powerup6_2);
		Pu_Anim_Disruption.AddImage(context, R.drawable.powerup6_3);
		Pu_Anim_Disruption.AddImage(context, R.drawable.powerup6_4);
		Pu_Anim_Disruption.AddImage(context, R.drawable.powerup6_5);
		Pu_Anim_Disruption.AddImage(context, R.drawable.powerup6_6);
		Pu_Anim_Disruption.AddImage(context, R.drawable.powerup6_7);
		Pu_Anim_Disruption.AddImage(context, R.drawable.powerup6_8);
		Pu_Anim_Player = new cAnimation(true, 0.15f);
		Pu_Anim_Player.AddImage(context, R.drawable.powerup7_1);
		Pu_Anim_Player.AddImage(context, R.drawable.powerup7_2);
		Pu_Anim_Player.AddImage(context, R.drawable.powerup7_3);
		Pu_Anim_Player.AddImage(context, R.drawable.powerup7_4);
		Pu_Anim_Player.AddImage(context, R.drawable.powerup7_5);
		Pu_Anim_Player.AddImage(context, R.drawable.powerup7_6);
		Pu_Anim_Player.AddImage(context, R.drawable.powerup7_7);
		Pu_Anim_Player.AddImage(context, R.drawable.powerup7_8);	
		
		//BlockSpecial
		Bl_Anim_Touched = new cAnimation(true, 0.10f);
		Bl_Anim_Touched.AddImage(context, R.drawable.specialbloc1_1);
		Bl_Anim_Touched.AddImage(context, R.drawable.specialbloc1_2);
		Bl_Anim_Touched.AddImage(context, R.drawable.specialbloc1_3);
		Bl_Anim_Touched.AddImage(context, R.drawable.specialbloc1_4);
		Bl_Anim_Touched.AddImage(context, R.drawable.specialbloc1_5);
		Bl_Anim_Touched.AddImage(context, R.drawable.specialbloc1_6);
	}
}
